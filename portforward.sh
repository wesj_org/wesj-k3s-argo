#!/bin/bash

ARGO_NS=argocd
ARGO_UI_SVC=argocd-server
LOCAL_PORT=8443

echo "Serving ${ARGO_UI_SVC} on https://localhost:${LOCAL_PORT}..."
kubectl -n $ARGO_NS port-forward service/${ARGO_UI_SVC} ${LOCAL_PORT}:443