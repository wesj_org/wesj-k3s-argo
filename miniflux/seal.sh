K8S_NAMESPACE=apps

kubeseal -n apps -o=yaml < admin-secret.yml > admin-secret-sealed.yml
kubeseal -n apps -o=yaml < db-secret.yml > db-secret-sealed.yml
