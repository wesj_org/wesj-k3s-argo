ARGO_NS=argocd
ARGO_VERSION=release-1.8
ARGO_MANIFEST=https://raw.githubusercontent.com/argoproj/argo-cd/${ARGO_VERSION}/manifests/install.yaml

kubectl create ns $ARGO_NS
kubectl apply -n $ARGO_NS -f $ARGO_MANIFEST

sleep 5

ARGO_ROOTLOGIN=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)

echo "LOGIN: admin / ${ARGO_ROOTLOGIN}"